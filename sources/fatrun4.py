import pyxel, random

pyxel.init(280, 280, title="FatRun", fps=60)

#POSITION JOUEUR
x = 127
y = 230

#POSITION OBSTACLES
obstacles_x = []
obstacles_y = []

#POSITION BONUS
bonus_x = []
bonus_y = []

score = 0 # initialisation des scores
jouer = False
gameOver = False
decalage = 0 # initialisation du déalage du fond d'écran

#JOUEUR
pyxel.image(0).load(0, 0, "joueur 2.png")
ljoueur = 26
hjoueur = 26

#BURGERS
pyxel.image(1).load(0, 0, "burger 2.png")
lburger = 26
hburger = 26

#CAROTTES
pyxel.image(1).load(0, 26, "carotte 2.png")
lcarotte = 26
hcarotte = 26

#FOND D'éCRAN
pyxel.image(2).load(0,0, "fond_ecran 1.png")
l = pyxel.image(2).width
h = pyxel.image(2).height

#GAME OVER
pyxel.image(0).load(0,26, "screen_game_over.png")
lg = 280
hg = 280

def deplacements():
  """
  la fonction deplacement ne prend pas d'argument et renvoie les variables x et y qui représente les mouvements du personnage principale,
  de plus elle permet au joueur de pouvoir déplacer le personnage a l'aide des croix directionnelle du clavier.
  Cependant il faut noter que si le joueur dépasse la carte sur le coté droit alors il fera apparition du coté gauche et inversement,
  d'autre part le joueur ne peux pas dépasser la carte sur sa hauteur.
  """
  global x,y
  if pyxel.btn(pyxel.KEY_RIGHT):
    x += 2
    if x >= 280-15:
      x = 0
  if pyxel.btn(pyxel.KEY_LEFT):
    x -= 2
    if x <= -5:
      x = 280-15
  if pyxel.btn(pyxel.KEY_UP):
    y -= 2
    if y <= 0 :
      y = 0
  if pyxel.btn(pyxel.KEY_DOWN):
    y += 2
    if y >= 280 - 23:
      y = 280 - 23
  return x, y

def obstacles():
  """
  La fonction obstacle ne prend pas de d'argument et renvoie la variable gameOver.
  La fonction ajoute un chiffre pseudo-aléatoire a la liste obstacles_x. Ce chiffre aléatoire représente la positition x de l'obstacle.
  La fonction ajoute un 0 à la liste obstacles_y. Ce 0 représente la positition y de l'obstacle.
  Ce même chiffre va augmenter de 1.5 en 1.5 pour que les obstacles descendent dans la carte
  """
  global gameOver, x, y
  if pyxel.frame_count % 30 == 0:
    obstacles_x.append(random.randint(0, 267))
    obstacles_y.append(-10)
  for i in range(len(obstacles_y)):
    obstacles_y[i] += 1.5
    if obstacles_y[i] + 12 >= y and obstacles_y[i] <= y + 12 and obstacles_x[i] + 12 >= x and obstacles_x[i] <= x + 12:
      gameOver = True
  return gameOver

def bonus():
  """
    La fonction bonus ne prend pas de d'argument et renvoie les variables : bonus_x, bonus_y, score.
    La fonction ajoute un chiffre pseudo-aléatoire a la liste bonus_x entre 0 et 280-13(soit la largeur du jeu moins la taille de l'obstacle).
    Ce chiffre aléatoire représente la positition x de l'obstacle.
    La fonction ajoute un 0 à la liste bonus_y. Ce 0 représente la positition y du bonus.
    Ce même chiffre va augmenter de 1 en 1 pour que les obstacles descendent dans la carte
  """
  global bonus_x, bonus_y, score
  if pyxel.frame_count % 60 == 0:
    bonus_x.append(random.randint(0, 267))
    bonus_y.append(-10)
  i = 0
  while i < len(bonus_y):
    bonus_y[i] += 1
    if bonus_y[i] > 280:
      del bonus_x[i]
      del bonus_y[i]
    if bonus_y[i] + 15 >= y and bonus_y[i] <= y + 15 and bonus_x[i] + 15 >= x and bonus_x[i] <= x + 15:
      score += 1
      del bonus_x[i]
      del bonus_y[i]
    i += 1
  return bonus_x, bonus_y, score

def menu():
  """
  La fonction menu ne prend pas d'argument et renvoie la variable jouer.
  La fonction Menu sert a lancer le jeu une fois que l'utilisateur appuie sur la touche ESPACE
  """
  global jouer
  if pyxel.btnp(pyxel.KEY_SPACE):
    jouer = True
  return jouer

def game_over():
  """
  La fonction game_over() permet d'arreter le jeu lorsque le joueur rencontre un obstacle.
  Elle vide les liste d'obstacles et de bonus et remet le joueur a sa position initiale.
  Si le joueur tape "q" alors il ferme la fenêtre du jeu
  """
  global gameOver, obstacles_x, obstacles_y, bonus_x, bonus_y,x, y, score
  if pyxel.btnp(pyxel.KEY_SPACE):
    gameOver = False
    obstacles_x.clear()
    obstacles_y.clear()
    bonus_x.clear()
    bonus_y.clear()
    score = 0
    x = 127
    y = 230
  elif pyxel.btnp(pyxel.KEY_Q):
    pyxel.quit()
  return gameOver, obstacles_x, obstacles_y

def update():
  """
  La fonction update est l'un des fonctions intégré a pyxel, celle ci est appelé plusieur fois par seconde pour mettre a jour les informations lié au jeu,
  comme la position des obstacles, des bonus ou encore du joueur
  """
  global decalage
  menu()
  if jouer == True and gameOver == False:
    deplacements()
    obstacles()
    bonus()
    decalage += 1
    if decalage == 280:
      decalage = 1
  if gameOver == True :
    game_over()

def draw():
  global gameOver

  if jouer == True:

    # écran noir
    pyxel.cls(0)

    # fond qui défile
    pyxel.blt(0, decalage-280, 2, 0, 0, l, h, 2)
    pyxel.blt(0, decalage, 2, 0, 0, l, h, 2)

    # tableau des scores
    pyxel.text(15, 13, f"score = {score}", 7)

    #OBSTACLES/CAROTTES
    for i in range(len(obstacles_x)):
      #CAROTTES
      pyxel.blt(obstacles_x[i], obstacles_y[i], 1, 0, 26, lcarotte, hcarotte,0)

    #BONUS/BURGER
    for i in range(len(bonus_x)):
      #BURGERS
      pyxel.blt(bonus_x[i], bonus_y[i], 1, 0, 0, lburger, hburger,0)

    #JOUEUR
    pyxel.blt(x, y, 0, 0, 0, ljoueur, hjoueur,0)

  else :
    # fond noir
    pyxel.cls(0)
    # menu d'entrer
    pyxel.text(100, 120, "PRESS SPACE TO START", 7)
  if gameOver == True :
    pyxel.cls(0)
    xg = 0
    yg = 0
    # écran de game over
    pyxel.blt(xg, yg, 0, 0, 26, lg, hg)
    # tableau des scores
    pyxel.text(130, 200, f"SCORE : {score}", 7)

pyxel.run(update, draw)